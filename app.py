# to run u can type """python3 app.py""" in terminal
# i use value iteration method to deploy dp on grid world
import random


class KiarashWorld(object):
    def __init__(self):
        # settings
        self.grid_width = 5
        self.grid_high = 5
        self.grid = list()
        self.terminal_states = [[0, 0], [4, 4]]  # you can put as many as u want, just make sure they are valid
        self.value_iteration_threshold = 0.0001  # it refers to epsilon (smaller amounts demand more iteration)
        self.learning_rate = 0.1
        self.step_reward = -1  # each step costs by 1
        self.blocked_cells = 1  # you can increase but for more than 3 cells grid can have no solution
        self.blocked_cells_list = list()  # a list that keeps track of blocked cells
        self.max_iter = 10000  # in case that algorithm didn't converges
        # end of settings

        # create blocked cells
        while self.blocked_cells > 0:
            _x = random.randint(0, self.grid_width-1)
            _y = random.randint(0, self.grid_high-1)
            if [_x, _y] in self.terminal_states:
                continue
            self.blocked_cells_list.append([_x, _y])
            self.blocked_cells -= 1

        # grid world initialization
        for a in range(self.grid_high):
            _ = list()
            for b in range(self.grid_width):
                if [a, b] in self.terminal_states:
                    _.append(0)
                    continue
                if [a, b] in self.blocked_cells_list:
                    _.append('******blocked******')
                    continue
                _.append(random.random())
            self.grid.append(_)

    # custom print for grid world
    def print_grid(self):
        for a in self.grid:
            for b in a:
                print('| {0: <20} '.format(b), end='')
            print('|')

    # returns list of possible moves of current position
    def possible_moves(self, x, y):
        _moves = list()
        if x-1 >= 0 and [x-1, y] not in self.blocked_cells_list:
            _moves.append([x-1, y])
        if x+1 < self.grid_width and [x+1, y] not in self.blocked_cells_list:
            _moves.append([x+1, y])
        if y+1 < self.grid_high and [x, y+1] not in self.blocked_cells_list:
            _moves.append([x, y+1])
        if y-1 >= 0 and [x, y-1] not in self.blocked_cells_list:
            _moves.append([x, y-1])

        return _moves

    # returns all v_s of all possible actions of current position
    def compute_v_s(self, x, y):
        values = list()
        moves = self.possible_moves(x, y)
        for action in moves:
            action_possibility = 1 / len(moves)
            reward = self.step_reward
            values.append(action_possibility * (reward + self.learning_rate * self.grid[action[0]][action[1]]))
        return values

    ####################################################################################################################
    # core function that computes optimal policy
    def find_optimal_policy(self):
        while True:
            if self.max_iter < 0:
                print("algorithm didn't converged please try with other settings")
                break
            self.max_iter -= 1
            delta = 0
            for row in range(self.grid_high):
                for col in range(self.grid_width):
                    if [row, col] in self.terminal_states or [row, col] in self.blocked_cells_list:
                        continue
                    v_s = self.grid[row][col]
                    _temp = v_s
                    v_s = sum(self.compute_v_s(row, col))
                    self.grid[row][col] = v_s
                    delta = max(delta, abs(_temp - v_s))

            if delta < self.value_iteration_threshold:
                break

        print('#' * 100)
        self.print_grid()


KiarashWorld().find_optimal_policy()
